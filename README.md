# Socket.io for golang

基于 [go-socket.io](https://github.com/googollee/go-socket.io) 修改的一个 Socket.io 服务端

## 一. 自定义 Parser 
```golang
    server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.SetEncoder(socketio.NewBinEncoder)
	server.SetDecoder(socketio.NewBinDecoder)
```
## 二. 自定义 错误处理
```golang
    server.SetOnError(func(err interface{}) {
		fmt.Println(err)
		debug.PrintStack()
	})
```
## 三. 自定义数据封包
example:有时候我们需要以binary方式传递protobuf对象
```golang
    server.SetOnAckResult(func(err error, retV []reflect.Value) ([]interface{}, error) {
		ret := make([]interface{}, len(retV))
		for i, v := range retV {
			vv := v.Interface()
			if vp, ok := vv.(proto.Message); ok && v.IsNil() == false {
				bt, err := proto.Marshal(vp)
				if err != nil {
					fmt.Println(err)
					return nil, err
				}
				kd := reflect.TypeOf(vp)
				if kd.Kind() == reflect.Ptr {
					kd = kd.Elem()
				}
				ret[i] = map[string]interface{}{
					"class": fmt.Sprintf("%v", kd),
					"bytes": &socketio.Attachment{Data: bytes.NewBuffer(bt)},
				}
			} else {
				ret[i] = vv
			}
		}
		if err != nil {
			ret = append([]interface{}{exception.New(fmt.Sprintf("%v", err))}, ret...)
		} else {
			ret = append([]interface{}{nil}, ret...)
		}
		return ret, nil
	})
```
## 四. 修复一些BUG
 1. attachment.go:86 当bytes为空时会触发panic(已修复) 
 2. handler.go:174 当ack请求无参数时 decoder未正常关闭(已修复)